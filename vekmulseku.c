/**********************************************************************                                                                                      
 * MPI-based matrix multiplication AxB=C                                                                                                                     
 *********************************************************************/


#define N                 2000      /* number of rows and columns in matrix */
#include <time.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>


double a[N][N],b[N],c[N];

main(int argc, char **argv)
{
	int rows,i,j,k;
	struct timeval start, stop;
	rows = N;
	gettimeofday(&start, 0);
	for (i=0; i<N; i++) {
		b[i]= 2.0;
		for (j=0; j<N; j++) {
        		a[i][j]= 1.0;
      		}
    	}
      	for (i=0; i<rows; i++) {
       		c[i] = 0.0;
        	for (j=0; j<N; j++)
          		c[i] = c[i] + a[i][j] * b[j];
      	}

	gettimeofday(&stop, 0);
	printf("Here is the result matrix:\n");
    /*for (i=0; i<N; i++) {
      printf("%6.2f   ", c[i]);
      printf ("\n");
    }*/

    	fprintf(stdout,"Time = %.6f\n\n",
         	(stop.tv_sec+stop.tv_usec*1e-6)-(start.tv_sec+start.tv_usec*1e-6));

}
